'use strict'

const UserSocket = use('App/Models/UserSocket')

class NotificationController {
  constructor ({ socket, request, auth }) {
    this.socket = socket
    this.request = request
    this.auth = auth

    UserSocket.create({
      socket_id: socket.id,
      user_id: auth.user.id
    })

    console.log('%s joined with %s socket id', auth.user.email, socket.id)
  }

  async onClose () {
    console.log('socket %s closed', this.socket.id)
    await UserSocket.query().where('socket_id', this.socket.id).delete()
  }
}

module.exports = NotificationController
