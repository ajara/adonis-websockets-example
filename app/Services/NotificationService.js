'use strict'

const Ws = use('Ws')
const Env = use('Env')
const User = use('App/Models/User')
const Notification = use('App/Models/Notification')
const Mail = use('Mail')

class NotificationService {
    

    static async updatedElement (element, model, user, message) {

        let notification = new Notification()
        notification.user_id = user.id
        notification.model_id = element.id
        notification.model = model
        notification.message = message
        notification.link = ''

        await notification.save()

        let sockets = user.getRelated('sockets')
        const sockets_ids = sockets.toJSON().map( socket => socket.socket_id)
        const topic = Ws.getChannel('notifications').topic('notifications')

        if (topic) {
            const data = {
                username: 'system',
                body: notification.message
            }
            topic.emitTo('message', data, sockets_ids)
        }

        console.log('Updated element', 'Notified')
    }
}

module.exports = NotificationService