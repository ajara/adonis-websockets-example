'use strict'

const crypto = require('crypto')

class Hash {

    register (Model, customOptions = {}) {
        const defaultOptions = {}
        const options = Object.assign(defaultOptions, customOptions)

        Model.prototype.hash = function () {
            const data = this.toJSON()            
            const hash = crypto.createHash('sha1').update(JSON.stringify(data)).digest('base64')
            return hash
        }
    }
}

module.exports = Hash
