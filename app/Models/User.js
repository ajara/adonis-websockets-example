'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * Hide password when querying users
   * 
   */
  static get hidden () {
    return ['password']
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }
  
  sockets () {
    return this.hasMany('App/Models/UserSocket')
  }
  
  favorites () {
    return this.hasMany('App/Models/Favorite')
  }

  passwordLink () {
    return this.hasOne('App/Models/CreatePasswordLink')
  }

  workspaces () {
    return this.belongsToMany('App/Models/Workspace')
        .withPivot(['workspace_admin'])
        .withTimestamps()
  }

  watched_reports () {
    return this
      .belongsToMany('App/Models/WatchedReport')
      .pivotTable('user_watched_reports')
  }

  watched_documents () {
    return this
      .belongsToMany('App/Models/WatchedDocument')
      .pivotTable('user_watched_documents')
  }

}

module.exports = User
