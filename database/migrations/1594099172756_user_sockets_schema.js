'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSocketsSchema extends Schema {
  up () {
    this.create('user_sockets', (table) => {
      table.increments()
      table.string('socket_id').index()
      table.integer('user_id').index()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_sockets')
  }
}

module.exports = UserSocketsSchema
