'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('name', 100).default('').nullable()
      table.string('lastname', 100).default('').nullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 80).nullable()
      table.boolean('is_super').default(false).notNullable() 
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
