'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreatePasswordLinksSchema extends Schema {
  up () {
    this.create('create_password_links', (table) => {
      table.increments()
      table.timestamps()
      table.integer('user_id').notNullable()
      table.boolean('used').notNullable().default(false)
      table.string('link',255).notNullable()
      table.datetime('expires_on').notNullable()
    })
  }

  down () {
    this.drop('create_password_links')
  }
}

module.exports = CreatePasswordLinksSchema
