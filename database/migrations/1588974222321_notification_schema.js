'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NotificationSchema extends Schema {
  up () {
    this.create('notifications', (table) => {
      table.increments()
      table.integer('user_id').notNullable()
      table.integer('model_id').notNullable()
      table.string('model',100).notNullable()
      table.string('message',255).nullable()
      table.string('link',255).nullable()
      table.text('data').nullable()
      table.datetime('readed_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('notifications')
  }
}

module.exports = NotificationSchema
