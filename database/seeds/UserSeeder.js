'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')

class UserSeeder {
  async run () {
    let user = new User()
    user.email = 'ajara@meatcode.cl'
    user.password = 'beefsteak'
    user.name = 'Augusto'
    user.lastname = 'Jara'
    user.is_super = 1
    user.save()
  }
}

module.exports = UserSeeder
