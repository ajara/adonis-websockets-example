let ws = null
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTU5Mjg0MzkwNCwiZXhwIjoxNTk0NjQzOTA0fQ.Jfo2xVi7c-RuTg6zIILoKTWBsKHdeJLsJLEz7ahHjgc'

$(function () {
  // Only connect when username is available
  if (window.token) {
    console.log(window.token)
    startChat()
  }
})

function startChat () {
  ws = adonis.Ws().withJwtToken(window.token).connect()

  ws.on('open', () => {
    $('.connection-status').addClass('connected')
    subscribeToChannel()
  })

  ws.on('error', () => {
    $('.connection-status').removeClass('connected')
  })
}

function subscribeToChannel () {
    const chat = ws.subscribe('notifications')
  
    chat.on('error', () => {
      $('.connection-status').removeClass('connected')
    })
  
    chat.on('message', (message) => {
      $('.messages').append(`
        <div class="message"><h3> ${message.username} </h3> <p> ${message.body} </p> </div>
      `)
    })
}

$('#message').keyup(function (e) {
    if (e.which === 13) {
        e.preventDefault()
    
        const message = $(this).val()
        $(this).val('')
    
        ws.getSubscription('chat').emit('message', {
            body: message
        })
        return
    }
})