const Event = use('Event')

Event.on('visited::indicator', 'Indicator.visited')
Event.on('visited::report', 'Report.visited')
Event.on('visited::document', 'Document.visited')