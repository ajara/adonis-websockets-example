'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')
Route.any('/logout','UserController.logout').middleware('auth:session')

Route.on('admin/login').render('login')
Route.post('admin/login','UserController.login')

Route.group(() => {
    Route.on('/').render('admin').as('admin')

    //Users
    Route.get('/users','UserController.index').as('admin.users')
    Route.get('/users/show/:idUser','UserController.show').as('admin.users.show')
    Route.get('/users/create','UserController.create').as('admin.users.create')
    Route.post('/users','UserController.store').as('admin.users.store')
    Route.get('/users/edit/:idUser','UserController.edit').as('admin.users.edit')
    Route.post('/users/edit/:idUser','UserController.update').as('admin.users.update')
    
    //FAQs
    Route.get('/faq','FaqController.index').as('admin.faqs')
    Route.get('/faq/show/:id','FaqController.show').as('admin.faqs.show')
    Route.get('/faq/create','FaqController.create').as('admin.faqs.create')
    Route.post('/faq','FaqController.store').as('admin.faqs.store')
    Route.get('/faq/edit/:id','FaqController.edit').as('admin.faqs.edit')
    Route.post('/faq/edit/:id','FaqController.update').as('admin.faqs.update')
    Route.get('/faq/delete/:id','FaqController.delete').as('admin.faqs.delete')
    Route.post('/faq/delete/:id','FaqController.destroy').as('admin.faqs.destroy')

    //Tutorials
    Route.get('/tutorial','TutorialController.index').as('admin.tutorials')
    Route.get('/tutorial/show/:id','TutorialController.show').as('admin.tutorials.show')
    Route.get('/tutorial/create','TutorialController.create').as('admin.tutorials.create')
    Route.post('/tutorial','TutorialController.store').as('admin.tutorials.store')
    Route.get('/tutorial/edit/:id','TutorialController.edit').as('admin.tutorials.edit')
    Route.post('/tutorial/edit/:id','TutorialController.update').as('admin.tutorials.update')
    Route.get('/tutorial/delete/:id','TutorialController.delete').as('admin.tutorials.delete')
    Route.post('/tutorial/delete/:id','TutorialController.destroy').as('admin.tutorials.destroy')

    Route.post('/tutorial/:id/button','TutorialController.addButton').as('admin.tutorials.button')
    Route.post('/tutorial/image/:id/remove','TutorialController.removeButton').as('admin.tutorials.button.delete')

}).prefix('admin').middleware('auth:session')

Route.group(() => {
    //Documents
    Route.get('/document','DocumentController.index').as('admin.documents')
    Route.get('/document/show/:id','DocumentController.show').as('admin.documents.show')
    Route.get('/document/create','DocumentController.create').as('admin.documents.create')
    Route.post('/document','DocumentController.store').as('admin.documents.store')
    Route.get('/document/edit/:id','DocumentController.edit').as('admin.documents.edit')
    Route.post('/document/edit/:id','DocumentController.update').as('admin.documents.update')
    Route.get('/document/delete/:id','DocumentController.delete').as('admin.documents.delete')
    Route.post('/document/delete/:id','DocumentController.destroy').as('admin.documents.destroy')

    Route.post('/document/:id/version','DocumentController.addVersion').as('admin.documents.version')
    Route.post('/document/image/:id/remove','DocumentController.removeVersion').as('admin.documents.version.delete')

}).prefix('admin').middleware('auth:session').namespace('Admin')

//Login
Route.post('login', 'UserController.login').middleware('guest')
Route.post('register', 'UserController.register').middleware('guest')
Route.post('refreshToken', 'UserController.refreshToken')
Route.get('createPassword/:link', 'UserController.createPassword').middleware('guest')
Route.post('resetPassword', 'UserController.resetPassword').as('resetPassword')

//Azure login
Route.get('/login/azure/callback','AuthAzureController.callback')
Route.get('/login/azure', ({response}) => {
    const Env = use('Env')
    const oauth = Env.get('AZURE_LOGIN_URL') + "/oauth2/authorize?response_type=code&client_id=" + Env.get('AZURE_APP_ID') + "&redirect_uri=" + Env.get('AZURE_REDIRECT')
    return response.redirect(oauth)
})

//User
Route.get('me', 'UserController.me').middleware('auth:session,jwt')
Route.get('api/cors', 'TesttingController.cors')
Route.on('chat').render('chat')

Route.group(() => {
    Route.get('/home','HomeController.index')

    Route.get('/indicator','IndicatorController.index')
    Route.get('/indicator/:id','IndicatorController.show')
    Route.patch('/indicator/:id','IndicatorController.update')
    Route.get('/indicator/:id/values','IndicatorController.values')
    Route.post('/indicator/:id/share','IndicatorController.share')

    Route.get('/report','ReportController.index')
    Route.get('/report/:id','ReportController.show')
    Route.patch('/report/:id','ReportController.update')
    Route.get('/report/:id/versions','ReportController.versions')
    Route.get('/report/:id_report/download/:id_version','ReportController.download')
    Route.post('/report/:id/share','ReportController.share')
    Route.post('/report/:id/watch','ReportController.watch')
    Route.delete('/report/:id/watch','ReportController.unwatch')

    Route.get('/document','DocumentController.index')
    Route.get('/document/:id','DocumentController.show')
    Route.patch('/document/:id','DocumentController.update')
    Route.get('/document/:id/versions','DocumentController.versions')
    Route.get('/document/:id_document/download/:id_version','DocumentController.download')
    Route.post('/document/:id/share','DocumentController.share')
    Route.post('/document/:id/watch','DocumentController.watch')
    Route.delete('/document/:id/watch','DocumentController.unwatch')
    
    Route.get('/favorite', 'FavoriteController.index')
    Route.get('/favorite/:type', 'FavoriteController.getOneTypeFavorite')
    Route.post('/favorite/:type/:id', 'FavoriteController.store')
    Route.delete('/favorite/:type/:id', 'FavoriteController.delete')

    Route.get('/recent', 'RecentController.index')
    Route.get('/recent/:type', 'RecentController.getOneTypeRecent')
    Route.post('/recent/:type/:id', 'RecentController.store')
    Route.delete('/recent/:type/:id', 'RecentController.delete')
    
    Route.get('/workspace','WorkspaceController.index')
    Route.get('/workspace/:id/type/:type', 'WorkspaceController.getOneTypeWorkspace')
    Route.get('/workspace/:id','WorkspaceController.show')
    Route.post('/workspace','WorkspaceController.store')
    Route.post('/workspace/:id/share','WorkspaceController.share')
    
    Route.post('/workspace/:id/add/:type/:object_id','WorkspaceController.addObject')
    Route.delete('/workspace/:id/remove/:type/:object_id','WorkspaceController.removeObject')
    
    Route.get('/shared', 'SharedController.index')
    Route.get('/shared/:type', 'SharedController.getOneTypeShared')
    Route.delete('/shared/:type/:id', 'SharedController.delete')
    
    Route.get('/faq', 'FaqController.query')

    Route.get('/departament', 'DepartamentController.departament')
    Route.get('/departament/:id/responsibles', 'DepartamentController.responsibles')
    
    Route.post('/help', 'HelpController.contact')

}).prefix('api').middleware('auth:jwt')